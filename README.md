# Rust Test exercices

Test exercices for Iaout

# Install Rust >= 1.58.0

Install the rust toolchain with [rustup](https://rustup.rs/).
To install the appropriate rust version, just run the command below in the
project's root directory after having installed `rustup`.

    rustc --version

# Build and run the program

    cargo run

# Build and run in release mode

    cargo run --release

# Launch tests

One test "rot13" does not works. There is a problem with the current implementation.

    cargo test
