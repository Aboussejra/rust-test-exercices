use std::collections::HashMap;

pub fn fib(n: usize) -> usize {
    // The Fibonacci sequence is a function taking natural numbers as arguments as the function defined here
    // Thus, if the function is called, the function will work
    if n == 0 {
        0
    } else if n == 1 {
        1
    } else {
        fib(n - 1) + fib(n - 2)
    }
}
// this recursive function is extremely inefficient (of time complexity O( φ ^n)
// where φ is the golden ratio, as many recursive calls are made on the same values of n

// The memoized version of fib stores the values already calculated in a Hashmap
pub fn memo_fib(n: usize) -> usize {
    let mut cache = HashMap::new();
    cache.insert(0, 0);
    cache.insert(1, 1);
    match n {
        0 => return 0,
        1 => return 1,
        // If one of the two cases up there are matched, no need to go further
        n => {
            for i in 2..=n {
                // We now the getting works due to function construction, we can unwrap safely

                // I know I am unnecessarily using a let, yet I found it clearer this way,
                // and allocating an usize for a bit more lifetime won't kill
                let fib_i_minus_2 = *cache
                    .get(&(i - 2))
                    .expect("This does exist or math is broken");
                let fib_i_minus_1 = *cache
                    .get(&(i - 1))
                    .expect("This does exist or math is broken");
                let fib_i = fib_i_minus_1 + fib_i_minus_2;
                cache.insert(i, fib_i);
            }
        }
    };
    *cache
        .get(&n)
        .expect("If we get here, normally Fibonnaci has been calculated")
}
