use rust_test_exercices::rot13::rot13;

fn main() {
    println!("Hello, world!");
    let _foo = rot13();
}
#[cfg(test)]
mod tests {
    use rust_test_exercices::{
        fibonacci::{fib, memo_fib},
        fizzbuzz::fizzbuzz,
    };
    #[test]
    fn testing_fizzbuzz() {
        // I could have wrote the same logic as in fizzbuzz function to test it
        // doing a for loop and calculating the remainders
        // It being the same implementation of the function, it did not seem smart.
        assert_eq!(fizzbuzz(1), "Nothing");

        assert_eq!(fizzbuzz(2), "Nothing");

        assert_eq!(fizzbuzz(3), "Fizz");

        assert_eq!(fizzbuzz(4), "Nothing");

        assert_eq!(fizzbuzz(5), "Buzz");
    }
    #[test]
    fn testing_fibonacci() {
        assert_eq!(fib(14), 377);
        // One could use Binet's Formula which is obtained by using basic mathematics
        // to solve linear recursion sequels, yet there is the problem
        // Of computation precision.
        // I did not want to take too much time on that, thuss I will settle on testing on a large number
        assert_eq!(memo_fib(14), 377)
        // One could test the speeds of both functions, to verify memo_fib is faster
    }
    use rust_test_exercices::rot13::rot13;
    #[test]
    fn test_rot13() {
        assert_eq!("This test fails", rot13())
    }
}
