use std::io::Write;
//This derive was added for comprehension purposes during coding
#[derive(Debug)]
struct Rot13Writer<T>
where
    T: Write,
{
    inner: T,
    pos: usize,
}

impl<T> Rot13Writer<T>
where
    T: Write,
{
    pub fn new(inner: T) -> Self {
        Rot13Writer { inner, pos: 0 }
    }
}

impl<T> Write for Rot13Writer<T>
where
    T: Write,
{
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        assert_eq!(self.pos, 0);
        let n = self.inner.write(buf)?;
        Ok(n)
    }

    fn flush(&mut self) -> std::io::Result<()> {
        // Not sure what this is needed for
        Ok(())
    }
}
// I know Rot13 is a particular case of Caesar Cipher
// Yet implementing the writers is not something I really know.
// I do not think I can solve the exercices perfectly without cheating on the internet.
// And this exercise's goal was clearly not only to implement Rot13's algorithm
// I obtain : "?U[\u{13}cXacked\u{13}m_\u{13}Y[VeX\u{13}diffic[lZ\u{13}cUdiTg\u{13}challeTge!!!\u{13}cUde\u{13}iY\u{13}WWb``Afg"
// One can see cracked, coding, challenge ect... I suppose it is partially correct.
// I must admit I do not know how to implement a Writer, I cannot take much more time to work on this.
// As I have my side work to work on. I would be pleased to know the perfect solution.
#[allow(clippy::unused_io_amount)]
pub fn rot13() -> String {
    let mut content = Vec::<u8>::default();
    let mut buff = Rot13Writer::new(&mut content);
    buff.write(b"Lbh penpxrq zl fhcre qvssvphyg pbqvat punyyratr... pbqr vf ddommNst")
        .unwrap();
    let result = content
        .iter()
        .map(|x| match *x {
            x if x <= 13 => (x + 13) as char,
            x if x >= 13 => (x - 13) as char,
            x => x as char,
        })
        .collect::<String>();
    println!("result: {:?}", result);
    result
}
