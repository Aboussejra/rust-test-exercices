/// This functions matches the remainder of n by 3 and 5
/// If n is divisible by 3 and 5, then FizzBuzz is printed
/// Else one of the remainder is equal to 0
/// Thus we match each of the cases.
pub fn fizzbuzz(n: usize) -> String {
    match (n % 3, n % 5) {
        // We convert the string slice (&str) to an owned String
        // It may be useful to pass it to other threads.
        // If we only need to view the result, a &str could have been used.
        (0, 0) => "FizzBuzz".to_string(),
        (0, _) => "Fizz".to_string(),
        (_, 0) => "Buzz".to_string(),
        (_, _) => "Nothing".to_string(),
    }
}
